# JSDoc,JSDoc3 備忘録
# [JSDoc](https://jsdoc.app/about-getting-started.html)
- JavaScriptの各APIアノテーション用マークアップ言語  
- /\*,/\*\*\*内でマークアップされたJSDocはJSDocとして無視される    
※マークアップ詳細は、index.jsにて確認
## [JSDocタグ](https://jsdoc.app/about-block-inline-tags.html#examples)
JSDocsには2種類のタグをサポートされている。  
設定可能なタグにBlockTagとInlineTagがある。
## Block tag
- 関数の引数等詳細情報を記述するタグ
- 接頭辞は、@(アットマーク)から始める
- 1ブロックタグ毎改行
## Inline tag
- テキストを{}(中括弧)で囲まれたタグ
- 改行不要
- @(アットマーク)で始まるタグもある
# [JSDoc3](https://github.com/jsdoc/jsdoc)
- JSDocを元としたAPIドキュメントジェネレーターnpmパッケージ
- npmパッケージ名は,jsdoc
- HTMLファイルでドキュメント生成される
- コマンド実行時のデフォルトドキュメント生成ディレクトリは、out/
## JSDoc3使用手順
## jsdoc(JSDoc3)インストール
```
npm i -g jsdoc
```
## 該当ファイル、ディレクトリ名指定しjsdoc実行
```
jsdoc [ファイル、ディレクトリ名]
```