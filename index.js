//JSDoc 説明用ファイル

/**
 * 合計を返す関数sum
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @returns {number} 合計
 */
function sum(a,b,c) {
	return a + b + c;
}